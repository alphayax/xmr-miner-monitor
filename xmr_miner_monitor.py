#! /usr/bin/env python3.6

from xmr_miner_monitor.BalanceController import BalanceController
from xmr_miner_monitor.Configuration import Configuration


if __name__ == '__main__':

    config = Configuration()
    config.load()

    balanceController = BalanceController(config=config)
    balanceController.run()

