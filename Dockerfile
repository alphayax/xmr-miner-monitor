FROM python:3.5
LABEL Maintainer="Alphayax <alphayax@gmail.com>"

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip install -r requirements.txt

COPY config.json /usr/src/app/
COPY xmr_miner_monitor.py /usr/src/app/
COPY xmr_miner_monitor /usr/src/app/xmr_miner_monitor


CMD [ "python", "./xmr_miner_monitor.py" ]
