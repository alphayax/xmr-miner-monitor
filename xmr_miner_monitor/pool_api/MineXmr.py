from json import JSONDecodeError

import requests


class MineXmr:

    @staticmethod
    def getCurrentBalance(wallet_address):
        """
        Get the current balance on minexmr.com

        :return: The current balance for the given address
        :rtype float
        """

        url = "https://p5.minexmr.com/stats_address"
        params = dict(
            address=wallet_address,
            longpoll='false',
        )

        try:
            resp = requests.get(url=url, params=params)
            contents = resp.json()
            balance = int(contents['stats']['balance'])

            return balance / 1000000000000

        except ConnectionError:
            print('Internet connexion seems to have gone away. Wallet cannot be updated')

        except JSONDecodeError:
            print('Error when decoding API response. Wallet cannot be updated')

        except requests.exceptions.SSLError:
            print('SSL connexion failed. Wallet cannot be updated')

