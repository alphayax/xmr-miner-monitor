#! /usr/bin/env python3.6

import json

from os import path


class Configuration:

    CONFIG_FILENAME = 'config.json'

    def __init__(self):
        self.wallet_address = ''
        self.nap_time = 360
        self.api = None

    def __readConfig(self):
        """
        Read the config file and load configuration

        :return: void
        """
        with open(Configuration.CONFIG_FILENAME, 'r') as config_file:
            config_x = json.load(config_file)
            self.wallet_address = config_x['wallet_address']
            self.nap_time = config_x['nap_time']

            if 'external_api' in config_x:
                self.api = config_x['external_api']

    def __writeConfig(self):
        """
        Write the current configuration into the config file

        :return: void
        """
        with open(Configuration.CONFIG_FILENAME, 'w') as config_file:
            json.dump({
                'wallet_address': self.wallet_address,
                'nap_time': self.nap_time
            }, config_file, indent=2, sort_keys=True)

    def load(self):
        """
        Try to load the configuration from the config file
        If config file does not exists, ask basic information and write the config file

        :return: void
        """
        if path.exists(Configuration.CONFIG_FILENAME):
            self.__readConfig()
            print('Configuration file loaded (' + Configuration.CONFIG_FILENAME + ')')
            self.print()
            return

        print('Configuration file not found. Initializing...')
        self.wallet_address = input('Wallet address : ')
        self.__writeConfig()
        print('')

    def print(self):
        """
        Writes on stdout the current configuration

        :return: void
        """
        print(" - Wallet : " + self.wallet_address)
        print(" - Nap time : " + str(self.nap_time) + "secs")

        if self.api is not None:
            print(" - External API : " + self.api['endpoint'])
