
import datetime
import sys
import time

import requests

from xmr_miner_monitor.CliColors import CliColors
from xmr_miner_monitor.Currency import Currency
from xmr_miner_monitor.EarningRate import EarningRate
from xmr_miner_monitor.Wallet import Wallet


class BalanceController:

    def __init__(self, config):
        self.config = config
        self.total_earned = 0
        self.wallet = Wallet(self.config.wallet_address)
        self.wallet.updateBalance()
        self.start_time = datetime.datetime.now()

    def waitUntilNextCheck(self):

        earning_rate = EarningRate(self.start_time, self.total_earned)

        for x in range(0, self.config.nap_time):
            remaining = self.config.nap_time - x
            print('[ SUMMARY ] {b}{g}{earned}{c} {earning_rate} - {remaining}      '.format(
                remaining=remaining,
                b=CliColors.BOLD,
                g=CliColors.OKGREEN,
                c=CliColors.ENDC,
                earned=Currency.format(self.total_earned),
                earning_rate=earning_rate.asString(),
            ), end="\r")
            time.sleep(1)

    def summarize(self):
        print('')
        print('---')

        # Total Earned
        print(' Total Earned : {b}{g}{earned}{c} '.format(
            b=CliColors.BOLD,
            g=CliColors.OKGREEN,
            c=CliColors.ENDC,
            earned=Currency.format(self.total_earned),
        ))

        # Time working
        elapsed = datetime.datetime.now() - self.start_time
        print(' Time elapsed : {b}{time_elapsed:.0f} secs{c}'.format(
            time_elapsed=elapsed.total_seconds(),
            b=CliColors.BOLD,
            c=CliColors.ENDC,
        ))

        # Earning rate
        earning_rate = EarningRate(self.start_time, self.total_earned)
        earning_rate.print()

    def run(self):
        print(" - Start date : " + datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))
        print(" - Current balance : {balance_relative} ({balance_absolute} XMR)".format(
            balance_relative=Currency.format(self.wallet.current_balance),
            balance_absolute=self.wallet.current_balance,
        ))
        print("")

        while 1:

            try:
                old_balance = self.wallet.current_balance

                self.waitUntilNextCheck()
                self.wallet.updateBalance()

                earned = self.wallet.current_balance - old_balance

                if earned > 0:
                    now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
                    self.total_earned += earned
                    print('[{now}] {g}{earned}{c}                                                              '.format(
                        now=now,
                        earned=Currency.format(earned),
                        g=CliColors.OKGREEN,
                        c=CliColors.ENDC,
                    ))

                    # If an external API is defined, we send collected data to it
                    if self.config.api is not None:
                        requests.post(url=self.config.api['endpoint'], json={
                            "amount": earned * 1000000,
                            "currency": "XMR",
                            "address": "test_address_XMM"
                        })

            except KeyboardInterrupt:
                self.summarize()
                sys.exit()
