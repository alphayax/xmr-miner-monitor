
class Currency:

    @staticmethod
    def format(balance):
        if balance < 0.001:
            return '{amount:.1f} µXMR'.format(amount=(balance * 1000 * 1000))

        if balance < 1:
            return '{amount:.1f} mXMR'.format(amount=(balance * 1000))

        return '{amount:.1f} mXMR'.format(amount=balance)
