
from xmr_miner_monitor.pool_api.MineXmr import MineXmr


class Wallet:

    def __init__(self, wallet_address):
        """
        Create a new wallet

        :param wallet_address: Address of wallet
        :type wallet_address: str
        """
        self.wallet_address = wallet_address
        self.current_balance = 0

    def updateBalance(self):
        """
        Get the current balance of the wallet

        :return: The current balance
        :rtype: float
        """

        self.current_balance = MineXmr.getCurrentBalance(self.wallet_address)
