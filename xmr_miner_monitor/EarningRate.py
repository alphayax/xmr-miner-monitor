
import datetime

from xmr_miner_monitor.Currency import Currency


class EarningRate:

    def __init__(self, start_time, total_earned):
        self.start_time = start_time
        self.total_earned = total_earned
        self.rate_m = 0
        self.rate_h = 0
        self.rate_d = 0
        self.compute()

    def compute(self):
        """
        Compute earning rate from the time elapsed and the total earned
        """

        elapsed_seconds = (datetime.datetime.now() - self.start_time).total_seconds()
        if elapsed_seconds == 0 or self.total_earned == 0:
            return

        self.rate_m = self.total_earned * 60 / elapsed_seconds
        self.rate_h = self.total_earned * 3600 / elapsed_seconds
        self.rate_d = self.total_earned * 3600 * 24 / elapsed_seconds

    def asString(self):
        """
        Return an string with the information about earning rate

        :return: A string like this : [{rate_m}/m - {rate_h}/h - {rate_d}/d]
        :rtype: str
        """

        return '[{rate_m}/m - {rate_h}/h - {rate_d}/d]'.format(
            rate_m=Currency.format(self.rate_m),
            rate_h=Currency.format(self.rate_h),
            rate_d=Currency.format(self.rate_d)
        )

    def print(self):
        """
        Print on stdout the information about earning rate
        """

        print(' Rates :')
        print('  - {rate_m}/m'.format(rate_m=Currency.format(self.rate_m)))
        print('  - {rate_h}/h'.format(rate_h=Currency.format(self.rate_h)))
        print('  - {rate_d}/d'.format(rate_d=Currency.format(self.rate_d)))
        print('')
